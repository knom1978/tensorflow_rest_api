The idea to demo tensorflow model with rest api
===
###### tags: `tensorflow-gpu` `rest api`
## Introduction
Currently containers are used in deep learning applications popularly. So it is meaningful to demo the rest api of using tensorflow models.

## Tensorflow-gpu solution
Just use docker-compose.yml to start the app.
```
docker-compose -f docker-compose.yml up --build
```

## Tensorflow-cpu solution
In order to test cpu version, the docker-compose-cpu.yml and servercpu.Dockerfile are generated. 
```
docker-compose -f docker-compose-cpu.yml up --build
```