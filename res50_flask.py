#import keras
#import numpy as np
from numpy import expand_dims
from keras.applications import resnet50
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from keras.models import load_model
import flask
import io
from PIL import Image

model_path = '/home/res50.h5'
filepath = '/home/cecil/machine_learning_api/'
filename = 'cat.jpg'

# Create Flask application and initialize Keras model 
app = flask.Flask(__name__) 
model = None

# Function to Load the model  
def load_a_model():
    # global variables, to be used in another function 
    global model
    resnet_model = load_model(model_path, compile = False)
    model = resnet_model

def prepare_image(image, target): 
    if image.mode != "RGB": 
        image = image.convert("RGB") 
    # Resize the image to the target dimensions 
    image = image.resize(target)  
    # PIL Image to Numpy array 
    image = img_to_array(image)  
    # Expand the shape of an array, 
    # as required by the Model 
    image = expand_dims(image, axis = 0)  
    # preprocess_input function is meant to 
    # adequate your image to the format the model requires 
    image = imagenet_utils.preprocess_input(image)  
    # return the processed image 
    return image 

def prepare_image2(image):
    original = load_img(image, target_size=(224, 224))
    numpy_image = img_to_array(original)
    image_batch = expand_dims(numpy_image, axis=0)
    processed_image = resnet50.preprocess_input(image_batch)
    return processed_image

# Now, we can predict the results. 
@app.route("/predict", methods =["POST"]) 
def predict():
    data = {} # dictionary to store result 
    data["success"] = False
    # Check if image was properly sent to our endpoint 
    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            image = flask.request.files["image"].read() 
            image = Image.open(io.BytesIO(image)) 
            image = prepare_image(image, target=(224, 224))
            predictions = model.predict(image)
            results = imagenet_utils.decode_predictions(predictions)
            data["predictions"] = []
            for (ID, label, probability) in results[0]: 
                r = {"label": label, "probability": float(probability)} 
                data["predictions"].append(r) 
            data["success"] = True
    return flask.jsonify(data)

if __name__ == "__main__": 
    print(("* Loading Keras model and Flask starting server..."
        "please wait until server has fully started")) 
    load_a_model() 
    app.run(host='0.0.0.0', port=5000, threaded = False) 
