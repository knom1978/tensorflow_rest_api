# simple_request.py 
import requests 
import sys
import time

URL = "http://server:5000/predict"
  
# provide image name as command line argument 
IMAGE_PATH = sys.argv[1]  

image = open(IMAGE_PATH, "rb").read() 
payload = {"image": image} 

'''  
# make request to the API 
request = requests.post(URL, files = payload).json() 
  
if request["success"]: 
    # Print formatted Result 
    print("% s % 15s % s"%("Rank", "Label", "Probability")) 
    for (i, result) in enumerate(request["predictions"]): 
        print("% d.    % 17s %.4f"%(i + 1, result["label"], 
            result["probability"])) 
  
    else: 
        print("Request failed")
'''
time.sleep(20)
while True:
    request = requests.post(URL, files = payload).json()
    print(request)
    time.sleep(5)
