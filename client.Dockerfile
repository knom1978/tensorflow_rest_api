FROM python:3.6-jessie
LABEL maintainer="Cecil Liu <knom1978@hotmail.com>"

RUN pip3 install --no-cache-dir requests

COPY client.py cat.jpg /home/
WORKDIR /home
CMD [ "python3", "/home/client.py cat.jpg" ]
